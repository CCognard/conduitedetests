public class Scenario1 {
    public static void main(String[] argv) {
        System.out.println("Scenario 1 launched...");

        KdbInputPile kdbInputPile = new KdbInputPile();
        ViewTopPile viewTopPile = new ViewTopPile();
        int i, number_good_results, number_tests_total;

        number_good_results = 0;
        number_tests_total = 10;

        for(i = 0; i < number_tests_total; i++) {
            kdbInputPile.actionCommande("PUSH " + i);

            if(viewTopPile.getTop() != null && viewTopPile.getTop() == i)
                number_good_results++;
        }

        if(number_good_results == number_tests_total)
            System.out.println("Scenario 1 : success !");
        else {
            System.out.println("Scenario 1 failed !");
            System.out.println("Good results : " + number_good_results + "/" + number_tests_total + ".");
        }
    }
}
