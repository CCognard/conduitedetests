public abstract class InputPileStrategy {
    public abstract void push(int entier);
    public abstract Integer pop();
    public abstract void clear();
    public abstract void actionCommande(String cmd);
}
